# MadelineProto - MTProto Proxy

[MadelineProto](https://github.com/danog/MadelineProto) is a PHP MTProto client, which allows to create a proxy server. MTProto proxies currently available in Telegram alpha builds [@tgbeta](https://t.me/tgbeta)
